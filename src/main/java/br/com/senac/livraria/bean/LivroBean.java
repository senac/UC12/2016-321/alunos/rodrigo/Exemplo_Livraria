/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.senac.livraria.bean;

import br.com.senac.livraria.banco.AutorDAO;
import br.com.senac.livraria.banco.LivroDAO;
import br.com.senac.livraria.entity.Autor;
import br.com.senac.livraria.entity.Livro;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Named;

/**
 *
 * @author Administrador
 */

@Named(value = "livroBean")
   @ViewScoped
public class LivroBean extends Bean{
    private  Livro livro;
    private LivroDAO dao;

   public LivroBean() {
    }
    
    @PostConstruct
    public void init() {
        this.livro = new Livro();
        this.dao = new LivroDAO();
    }
    
    public String getCodigo() {
        return this.livro.getId() == 0 ? "" : String.valueOf(this.livro.getId());
    }
    
    public void novo() {
        this.livro = new Livro();
    }
    
    public void salvar() {
        
        try {
            
            if (this.livro.getId() == 0) {
                dao.save(livro);
                addMessageInfo("Salvo com sucesso!");
            } else {
                dao.update(livro);
                addMessageInfo("Alterado com sucesso!");
            }
            
        } catch (Exception ex) {
            addMessageInfo(ex.getMessage());
        }
        
    }
    
    public void excluir(Livro livro) {
        try {
            
            dao.delete(livro.getId());
            addMessageInfo("Removido com sucesso!");
            
        } catch (Exception ex) {
            addMessageErro(ex.getMessage());
        }
    }
 // A partir daqui   
    public Livro getLivro() {
        return livro;
    }
    
    public void setLivor(Livro livro) {
        this.livro = livro;
    }
    
    public List<Livro> getLista() {
        return this.dao.findAll();
    }
    
}
